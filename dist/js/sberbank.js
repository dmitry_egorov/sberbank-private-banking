;$(function() {
	$('.page-dot').click(function() {
		if ($(this).is('.active'))
			return;

		var page = $(this).data('page');
		$('.promo-features-container').animate({
			'margin-left': -(page * 1185)
		}, 800);

		$('.page-dot').removeClass('active');

		$(this).addClass('active');
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > 132) {
			$('nav.top-menu').addClass('fixed');
			$('div.header-line').addClass('fixed');
		} else {
			$('nav.top-menu').removeClass('fixed');
			$('div.header-line').removeClass('fixed');
		}
	});
});;$(function() {
	$('span.services-list-dropdown-arrow').click(function() {
		var $item = $(this).parents('.services-list-item');

		if ($item.is('.active')) {
			$item.removeClass('active');
			$item.find('div.services-list-dropdown').slideUp('fast').removeClass('active');
			$item.find('span.services-list-dropdown-arrow').removeClass('active');
		} else {
			$active = $('.services-list-item.active');
			$active.removeClass('active');
			$active.find('div.services-list-dropdown').slideUp('fast').removeClass('active');
			$active.find('span.services-list-dropdown-arrow').removeClass('active');

			$item.addClass('active');
			$item.find('div.services-list-dropdown').slideDown('fast').addClass('active');
			$item.find('span.services-list-dropdown-arrow').addClass('active');
		}
	});

	$('span.features-list-item-title').click(function() {
		var item = $(this).parents('.features-list-item');
		$(this).toggleClass('active');
		item.find('div.features-list-item-dropdown').slideToggle('fast').toggleClass('active');
		item.find('span.features-list-item-arrow').toggleClass('active');
	});

	$('a.locked').click(function(){
		$('div.password-form-container').show();
		return false;
	});

	$('div.password-form-close').click(function(){
		$('div.password-form-container').hide();
	});

	$('div.banking').click(function() {
		$('div.question-dropdown').slideUp('fast');

		$(this)
			.toggleClass('active')
			.find('div.banking-dropdown').slideToggle('fast');
	});

	$('div.question, input.question-dropdown-button').click(function() {
		$('div.banking')
			.removeClass('active')
			.find('div.banking-dropdown').slideUp('fast');
		$('div.question-dropdown').slideToggle('fast');
	});

	var $bg = $('div.bg');

	if (!$('body.error-404').length) {
		var bgId = getCookie('bg') !== 'undefined' ? getCookie('bg') : 1;
		$bg.css('background-image', 'url(dist/images/bg/' + bgId + '.jpg)');
	}
		
	if ($('body.sections').length) {

		$.fn.preload = function() {
			this.each(function() {
				$('<img/>')[0].src = this;
			});
		}

		$(['dist/images/bg/2.jpg', 'dist/images/bg/3.jpg', 'dist/images/bg/4.jpg', 'dist/images/bg/5.jpg']).preload();
	}

	$('div.sections a').mouseover(function() {
		var bgId = $(this).data('bg');
		if(bgId)
		$bg.css('background-image', 'url(dist/images/bg/' + bgId + '.jpg)');
	}).click(function() {
		var bgId = $(this).data('bg');
		setCookie('bg', bgId);
	});
});

function setCookie(c_name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start, c_end));
	}
	return c_value;
}