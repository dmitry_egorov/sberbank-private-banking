$(function() {
	$('.page-dot').click(function() {
		if ($(this).is('.active'))
			return;

		var page = $(this).data('page');
		$('.promo-features-container').animate({
			'margin-left': -(page * 1185)
		}, 800);

		$('.page-dot').removeClass('active');

		$(this).addClass('active');
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > 132) {
			$('nav.top-menu').addClass('fixed');
			$('div.header-line').addClass('fixed');
		} else {
			$('nav.top-menu').removeClass('fixed');
			$('div.header-line').removeClass('fixed');
		}
	});
});